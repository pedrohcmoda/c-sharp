﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="pratica2.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Criacao de Conta</title>
    <link href="Content/css/style.css" rel="stylesheet" />
</head>
<body>
    <table runat="server" class="tabelaPadrao">
        <tr>
            <td id="panelPessoal" runat="server">
                <asp:Panel runat="server">
                    <form id="formPessoal" runat="server">
                        <h2>Informações Pessoais</h2>
                        <div class="divisao" >
                            <asp:Label Text="Nome: " runat="server" />
                            <asp:TextBox ID="txtNome"  runat="server" Required="true" />
                        </div>
                        <div class="divisao" >
                            <asp:Label Text="Sobrenome: " runat="server" />
                            <asp:TextBox ID="txtSobrenome" runat="server" Required="true"/>
                        </div>
                        <div class="divisao">
                            <asp:Label Text="Genero: " runat="server" />
                            <asp:DropDownList ID="ddlGenero" AutoPostBack="true" runat="server" OnSelectedIndexChanged="mudaGenero">
                                <asp:ListItem Text="Masculino" Value="M" />
                                <asp:ListItem Text="Feminino" Value="F"/>
                                <asp:ListItem Text="Outro" Value="O"/>
                                <asp:ListItem Text="Prefiro não dizer" Value="P"/>
                            </asp:DropDownList>
                        </div>
                        <div class="divisao" id="outroGenero" runat="server" > 
                            <asp:Label Text="Digite o genero: " runat="server" />
                            <asp:TextBox ID="txtGenero"  runat="server"/>
                        </div>
                        <div class="divisao" >
                            <asp:Label  Text="Celular: " runat="server" />
                            <asp:TextBox ID="txtCelular" TextMode="Phone" runat="server" Required="true" />
                        </div>
                        <asp:Button class="btnPadrao" OnClick="proximo" Text="Proximo" runat="server" />
                    </form>
                </asp:Panel>
            </td>
        
            <td id="panelEndereco" runat="server">
                <asp:Panel runat="server">
                    <form id="formEndereco" runat="server">
                        <h2>Detalhes do endereço</h2>
                        <div class="divisao">
                            <asp:Label Text="Endereço: " runat="server" />
                            <asp:TextBox ID="txtEndereco" runat="server" Required="true"/>
                        </div>
                        <div class="divisao">
                            <asp:Label Text="Cidade: " runat="server" />
                            <asp:TextBox ID="txtCidade" runat="server" Required="true" />
                        </div>
                        <div class="divisao">
                            <asp:Label Text="CEP: " runat="server" />
                            <asp:TextBox ID="txtCep" runat="server" Required="true"/>
                        </div>
                        <asp:Button OnClick="anterior" Text="Anterior" UseSubmitBehavior="false" runat="server" />
                        <asp:Button OnClick="proximo" Text="Proximo" runat="server" />
                    </form>
                </asp:Panel>
            </td>
            <td id="panelLogin" runat="server">
                <asp:Panel runat="server">
                    <form id="formLogin" runat="server">
                        <h2>Área de Login</h2>
                        <div class="divisao">
                            <asp:Label Text="Usuario: " runat="server"  />
                            <asp:TextBox ID="txtUsuario" runat="server" Required="true" />
                        </div>
                        <div class="divisao">
                            <asp:Label Text="Senha: " runat="server" />
                            <asp:TextBox Textmode="Password" ID="txtSenha" runat="server" Required="true"/>
                        </div>
                        <asp:Button OnClick="anterior" Text="Anterior" UseSubmitBehavior="false" runat="server" />
                        <asp:Button OnClick="proximo" Text="Proximo" runat="server" />
                    </form>
                </asp:Panel>
            </td>
        </tr>
    </table>
</body>
</html>
