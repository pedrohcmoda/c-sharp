﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pratica2
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                outroGenero.Visible = false;
                panelEndereco.Visible = false;
                panelLogin.Visible = false;
            }
        }

        protected void proximo(object sender, EventArgs e)
        {
            if (panelPessoal.Visible == true)
            {
                panelPessoal.Visible = false;
                panelEndereco.Visible = true;
            }
            else if (panelEndereco.Visible == true)
            {
                panelEndereco.Visible = false;
                panelLogin.Visible = true;
            }
        }

        protected void anterior(object sender, EventArgs e)
        {
            if (panelLogin.Visible == true)
            {
                panelLogin.Visible = false;
                panelEndereco.Visible = true;
            }
            else if (panelEndereco.Visible == true)
            {
                panelEndereco.Visible = false;
                panelPessoal.Visible = true;
            }
        }

        protected void mudaGenero(object sender, EventArgs e)
        {
            if (ddlGenero.SelectedValue == "O")
            {
                outroGenero.Visible = true;
                txtGenero.Attributes.Add("required", "true");
            }
            else
            {
                outroGenero.Visible = false;
                txtGenero.Attributes.Remove("required");
            }
        }
    }
}