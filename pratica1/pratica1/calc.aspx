﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calc.aspx.cs" Inherits="pratica1.calc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CALCULADORA</title>
</head>
<body>
    <h1>Calculadora</h1>
    <form id="calculadora" runat="server">
        <div>
            <asp:TextBox runat="server" ID="txtValor1"/> 
            <asp:DropDownList runat="server" ID="ddlOperacoes">
                <asp:ListItem Text="+" Value="adicao" />
                <asp:ListItem Text="-" Value="subtracao"/>
                <asp:ListItem Text="/" Value="divisao"/>
                <asp:ListItem Text="*" Value="multiplicacao"/>
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtValor2"/>
        </div>
        <asp:Button runat="server" Text="Calcular" ID="btnCalcular" OnClick="calcular"/>
        <asp:Label ID="txtResposta" runat="server"></asp:Label>
    </form>
</body>
</html>
