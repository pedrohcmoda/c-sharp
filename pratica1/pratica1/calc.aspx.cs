﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pratica1
{
    public partial class calc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void calcular(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                double caixa1 = double.Parse(txtValor1.Text);
                double caixa2 = double.Parse(txtValor2.Text);
                double resultado = 0;
                string escolha = ddlOperacoes.SelectedValue;
                switch (escolha)
                {
                    case "adicao":
                        resultado = caixa1 + caixa2;
                        break;
                    case "subtracao":
                        resultado = caixa1 - caixa2;
                        break;
                    case "divisao":
                        resultado = caixa1 / caixa2;
                        break;
                    case "multiplicacao":
                        resultado = caixa1 * caixa2;
                        break;
                }
                txtResposta.Text = resultado.ToString();
            }
        }}
    }
