﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiasSemanais.aspx.cs" Inherits="pratica1.DiasSemanais" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="formDiasSemanais" runat="server">
        <h2>Selecione os dias disponiveis para trabalho</h2>
        <div>
            <asp:CheckBoxList runat="server" ID="cblDiasDaSemana">
                <asp:ListItem Text="Segunda-feira" />
                <asp:ListItem Text="Terça-feira" />
                <asp:ListItem Text="Quarta-feira" />
                <asp:ListItem Text="Quinta-feira" />
                <asp:ListItem Text="Sexta-feira" />
            </asp:CheckBoxList>
        </div>
        <asp:Button Text="Pronto" runat="server" OnClick="diasSelecionados" />
    </form>
    <br />
    <asp:Label ID="diasDaSemana" runat="server" />
</body>
</html>
